<?php

require_once "vendor/autoload.php";

#=======================================================

class Tyre 
{

    public float $pressure;

    public function __construct(float $pressure)
    {
        $this->pressure = $pressure;
    }
}

#=======================================================

abstract class Engine 
{
    public function start()
    {
        dump('Engine started...');
    }
}
#--------------------------------------
class ElectricEngine extends Engine
{

    public int $hoursePower;

    public function __construct(int $hoursePower)
    {
        $this->hoursePower = $hoursePower;
    }
}
#--------------------------------------
class DieselEngine extends Engine
{
    public int $cylinders;

    public function __construct(int $cylinders)
    {
        $this->cylinders = $cylinders;
    }
}

#=======================================================

class Tank
{

    public int $capacity;

    public function __construct(int $capacity)
    {
        $this->capacity = $capacity;
    }

}

#-------------------------------------------

class Battery
{
    public int $capacity;

    public function __construct(int $capacity)
    {
        $this->capacity = $capacity;
    }
}

#========================================================


abstract class Car
{

    public Engine $engine;
    
    public Tyre $frontLeftTyre;
    
    public Tyre $frontRightTyre;
    
    public Tyre $backLeftTyre;

    public Tyre $backRightTyre;

    public function __construct(
        Engine $engine,
        Tyre $frontLeftTyre,
        Tyre $frontRightTyre,
        Tyre $backLeftTyre,
        Tyre $backRightTyre
    ) {
        $this->engine = $engine;
        $this->frontLeftTyre = $frontLeftTyre;
        $this->frontRightTyre = $frontRightTyre;
        $this->backLeftTyre = $backLeftTyre;
        $this->backRightTyre = $backRightTyre;
    }

    public function run()
    {
        $this->engine->start();
    }
}

#========================================================

class DieselCar extends Car
{
    public Tank $tank;
    
    public function __construct(
        Tank $tank,
        Engine $engine,
        Tyre $frontLeftTyre,
        Tyre $frontRightTyre,
        Tyre $backLeftTyre,
        Tyre $backRightTyre
    ) {
        $this->tank = $tank;

        parent::__construct(
            $engine,
            $frontLeftTyre,
            $frontRightTyre,
            $backLeftTyre,
            $backRightTyre 
        );
    }
}

#------------------------------------------------- 

class ElectricCar extends Car
{
    public Battery $battery;
    
    public function __construct(
        Battery $battery,
        Engine $engine,
        Tyre $frontLeftTyre,
        Tyre $frontRightTyre,
        Tyre $backLeftTyre,
        Tyre $backRightTyre
    ) {
        $this->battery = $battery;

        parent::__construct(
            $engine,
            $frontLeftTyre,
            $frontRightTyre,
            $backLeftTyre,
            $backRightTyre 
        );
    }
}

$electricCar = new ElectricCar(
    new Battery(50000),
    new ElectricEngine(100),
    new Tyre(30),
    new Tyre(30),
    new Tyre(30),
    new Tyre(30)
);

$dieselCar = new DieselCar(
    new Tank(50),
    new DieselEngine(4),
    new Tyre(30),
    new Tyre(30),
    new Tyre(30),
    new Tyre(30)
);

dump($electricCar, $dieselCar);
