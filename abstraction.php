<?php

require_once "vendor/autoload.php";

# Abstraction

class Book 
{

    public int $numberOfPages;

    public string $title;

    public function __construct(int $numberOfPages, string $title)
    {
        $this->numberOfPages = $numberOfPages;
        $this->title = $title;
    }
}



class BookShelf
{
    public array $books = [];

    public function addBook(Book $book)
    {
        $this->books[] = $book;
    }

    public function search(string $title)
    {
        foreach($this->books as $book){
            if ($book->title == $title) {
                return $book;
            }
        }
    }
}

$bookShelf = new BookShelf();

$bookShelf->addBook(new Book(150, 'xyz'));
$bookShelf->addBook(new Book(250, 'abc'));

dump($bookShelf->search('ghi'));