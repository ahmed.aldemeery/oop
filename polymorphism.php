<?php

require_once "vendor/autoload.php";

abstract class Engine
{
    abstract function start();
}

class ElectricEngine extends Engine
{
    public function start()
    {
        dump("Starting using electricity...");
    }
}

class DieselEngine extends Engine
{
    public function start()
    {
        dump("Starting using diesel...");
    }
}

function run(Engine $engine)
{
    $engine->start();
}

$ec = new ElectricEngine();
$dc = new DieselEngine();

run($dc);
