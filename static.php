<?php

require_once "vendor/autoload.php";

class Counter
{
    private static int $number = 0;
    
    public static string $name;

    public function __construct(string $name)
    {
        self::$name = $name;
    }

    public function next()
    {
        self::$number += 1;
    }

    public function getNumber()
    {
        return self::$number;
    }

    public static function getName()
    {
        return self::$name;
    }
}

$ahmed = new Counter('Ahmed');
$osama = new Counter('Osama');
$ali = new Counter('Ali');

dump(Counter::$name);