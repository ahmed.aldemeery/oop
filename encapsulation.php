<?php

require_once "vendor/autoload.php";

class Engine
{
    private int $hoursePower;

    public function __construct(int $hoursePower)
    {
        $this->hoursePower = $hoursePower;
    }
}

class Car
{
    private bool $started = false;

    private Engine $engine;

    public function __construct(Engine $engine)
    {
        $this->engine = $engine;
    }

    public function start()
    {
        $this->started = true;
    }

    public function stop()
    {
        $this->started = false;
    }

    public function setEngine(Engine $engine)
    {
        if ($this->started == false) {
            $this->engine = $engine;
        } else {
            dump("Stop the car first");
        }
    }
}

$car = new Car(new Engine(120));
$car->start();
$car->stop();
$car->setEngine(new Engine(240));
$car->start();

dd($car);