<?php

require_once "vendor/autoload.php";

class Engine
{
    protected int $cylinders;

    public const TYPE = 'v8';

    public function __construct(int $cylinders)
    {
        $this->cylinders = $cylinders;
    }

    final public function start()
    {
        dump("Starting engine...");
    }
}

class ElectricEngine extends Engine
{
}

$engine = new Engine(8);
dd(Engine::TYPE);
