<?php

require_once "vendor/autoload.php";

# Inheritance

abstract class Mamal
{
    public function giveBirth()
    {
        dump("Giving birth");
    }

    abstract public function breastFeed();
}

class Human extends Mamal
{
    # Overriding
    public function giveBirth()
    {
        parent::giveBirth();
        dump("As a human I am giving birth");
    }
    
    public function talk()
    {
        dump("I am talking");
    }

    public function breastFeed()
    {
        dump("Human breast feeding");
    }
}

class Cow extends Mamal
{
    public function moo()
    {
        dump("Moooooooooo!");
    }

    public function breastFeed()
    {
        dump("Cow breast feeding");
    }
}

$human = new Human();
$cow = new Cow();

$human->giveBirth();
$human->talk();
$human->breastFeed();

dump("======================");

$cow->giveBirth();
$cow->moo();
$cow->breastFeed();

dump("======================");

// $mamal = new Mamal();
// $mamal->giveBirth();