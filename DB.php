<?php

require_once "vendor/autoload.php";

class DB 
{
    private string $table = "";
    
    private string $where = "";
    private mysqli $connection;
    
    public function __construct(mysqli $connection)
    {
        $this->connection = $connection;
    }
    
    public function table(string $table)
    {
        $this->table = $table;

        return $this;
    }
    
    public function create(array $attributes)
    {
        $query = "INSERT INTO +table+(+keys+) VALUES (+values+);";
        $query = str_replace("+table+", $this->table, $query);
        $query = str_replace("+keys+", implode(", ", array_keys($attributes)), $query);
        
        $values = "'" . implode("', '", array_values($attributes)) . "'";
        $query = str_replace("+values+", $values, $query);
        
        
        $this->perform($query);
        
        $id = mysqli_insert_id($this->connection);
        
        return $this->where('id=' . $id)->select('*')[0];
    }

    public function select(string $select)
    {
        $query = "SELECT +select+ FROM +table+";
        $query = str_replace("+select+", $select, $query);
        $query = str_replace("+table+", $this->table, $query);

        if (!empty($this->where)) {
            $query .= " WHERE " . $this->where;
        }

        $query .= ";";
        
        return mysqli_fetch_all($this->perform($query), MYSQLI_ASSOC);
    }

    public function where(string $where)
    {
        $this->where = $where;

        return $this;
    }

    public function update(array $attributes)
    {
        $query = "UPDATE +table+ SET ";
        $query = str_replace("+table+", $this->table, $query);

        foreach ($attributes as $key => $value) {
            $query .= "$key = '$value', ";
        }

        $query = rtrim($query, ", ");

        if (!empty($this->where)) {
            $query .= " WHERE " . $this->where;
        }

        $query .= ";";
        
        $this->perform($query);
    }

    public function delete()
    {
        $query = "DELETE FROM +table+ ";
        $query = str_replace("+table+", $this->table, $query);

        if (!empty($this->where)) {
            $query .= " WHERE " . $this->where;
        }

        $query .= ";";

        $this->perform($query);
    }

    private function perform(string $query)
    {
        return mysqli_query($this->connection, $query);
    }
}

$db = new DB(mysqli_connect('localhost', 'root', 'password', 'notes'));
$note = $db->table('notes')->create(['title' => 'GYM', 'content' => 'hi', 'position' => 1, 'user_id' => 1]);

$db->table('notes')->create(['title' => 'Ahmed', 'position' => 1, 'user_id' => 4]);

$rows = $db->table('notes')->where('user_id = 1')->select('title, content');

$db->table('notes')->where('id = 5')->update(['title' => 'new title', 'position' => 1]);
$db->table('notes')->where('id = 8')->delete();

$db = new DB(mysqli_connect('localhost', 'root', 'password', 'projects'));

// ==============================

$query = "INSERT INTO notes (user_id, name, position) VALUES (?, ?, ?);";
$pdo = new PDO('mysql:dbname=notes;host=localhost', 'root', 'password');
$pdo->prepare($query)->execute([1, "'; drop table notes;", 1]);


